<?php
/**
 * Created by PhpStorm.
 * User: Paul's
 * Date: 02.07.2018
 * Time: 19:12
 */

namespace App;



use Illuminate\Support\Facades\Redis;

class Trending
{

    public function get(){
       return array_map('json_decode',Redis::zrevrange($this->cacheKey(),0,5));
    }
    public function push($thread){
        Redis::zincrby($this->cacheKey(),1,json_encode([
            'title' => $thread->title,
            'path' => $thread->path()
        ]));
    }
    public function reset(){
        Redis::del($this->cacheKey());
    }
    private function cacheKey(){
        return 'trending_threads';
    }
}