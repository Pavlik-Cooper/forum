<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use RecordsActivity, Favoritable;

    protected $guarded = [];
    protected $appends = ['FavoritesCount', 'IsFavorited', 'IsBest'];
    protected $with = ['owner', 'favorites'];
    protected $touches = ['thread'];

    public function path()
    {
        return $this->thread->path() . "#reply-{$this->id}";
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }
    public function isBest(){
        return $this->thread->best_reply_id == $this->id;
    }
    public function getIsBestAttribute(){
        return $this->isBest();
    }
    public function wasJustPublished(){
        // $this->created_at > than now minus one minute
        return $this->created_at->gt(Carbon::now()->subMinute());
    }
    public function mentionedUsers()
    {
        preg_match_all('/@([\w\-]+)/', $this->body, $matches);
        return $matches[1];
    }
    public function setBodyAttribute($body)
    {
       $this->attributes['body'] = preg_replace('/@([\w\-]+)/','<a href="/profiles/$1">$0</a>', $body);
    }
    protected static function boot(){
        static::deleted(function ($reply){
            if ($reply->isBest()){
                $reply->thread->update(['best_reply_id'=>null]);
            }
        });
    }
}