<?php

namespace App;

use App\Events\ThreadHasNewReply;
use App\Filters\ThreadFilters;
use App\Notifications\ThreadWasUpdated;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Thread extends Model
{
    //

    use RecordsActivity;//Searchable;


    // from Searchable trait
    public function toSearchableArray()
    {
        return $this->toArray() + ['thread_path'=> $this->path()];
    }
    protected $guarded = [];
    protected $with = ['creator', 'channel'];
    protected $appends = ['IsSubscribed'];

    public function scopeFilter($query, ThreadFilters $filters){
//        dd($query->toSql());
        return $filters->apply($query);
    }

    public function path(){
        return "/threads/{$this->channel->slug}/{$this->id}";
    }
    public function subscribe($user_id = null){
        return $this->subscriptions()->create(['user_id' => $user_id ?: auth()->id()]);
    }
    public function unsubscribe($user_id = null){
        return $this->subscriptions()->where('user_id', $user_id ?: auth()->id())->delete();
    }
    public function replies(){
        return $this->hasMany(Reply::class);
//        return $this->hasMany(Reply::class)->withCount('favorites')->with('owner');
    }
    public function subscriptions(){
        return $this->hasMany(ThreadSubscription::class);
    }
    public function creator(){
       return $this->belongsTo(User::class,'user_id');
    }

    public function addReply($reply){
        $reply = $this->replies()->create($reply);

        event(new ThreadHasNewReply($reply));

        return $reply;
    }

    public function channel(){
        return $this->belongsTo(Channel::class);
    }
    protected static function boot()
    {
        parent::boot();
         // thread replies count for all the routs
        static::addGlobalScope('replyCount',function ($builder){
            $builder->withCount('replies');
        });
        static::deleting(function ($thread){
            // in order to trigger RecordsActivity trait static::deleting() method
            // to delete activity for each reply. Simple $thread->replies()->delete() doesn't work here
            $thread->replies->each->delete();
        });
    }
    public function getIsSubscribedattribute(){
        return $this->subscriptions()->where('user_id',auth()->id())->exists();
    }
    public function hasUpdatesFor($user){
        $key = $user->visitedThreadCacheKey($this);
        return $this->updated_at > cache($key);
    }
    public function visits(){
        return new Visits($this);
    }
    public function markBestReply(Reply $reply){
        $this->update(['best_reply_id'=>$reply->id]);
    }
}
