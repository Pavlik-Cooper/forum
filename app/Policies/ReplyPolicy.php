<?php

namespace App\Policies;

use App\Reply;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReplyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function update(User $user, Reply $reply){
        return $user->id == $reply->user_id;
    }
    public function create(User $user){
        // if there's no replies at all in that particular user
       if(! $lastReply = $user->lastReply) return true;
        return ! $lastReply->wasJustPublished();
    }
}
