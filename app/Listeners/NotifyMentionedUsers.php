<?php

namespace App\Listeners;

use App\Events\ThreadHasNewReply;
use App\Notifications\YouWereMentioned;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyMentionedUsers
{

    public function handle(ThreadHasNewReply $event)
    {
        //
        User::whereIn('name',$event->reply->mentionedUsers())->get()
            ->each->notify(new YouWereMentioned($event->reply));
    }
}
