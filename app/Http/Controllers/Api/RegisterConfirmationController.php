<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterConfirmationController extends Controller
{
    //
    public function index(){
       $user = User::where('confirmation_token',request('token'))
            ->firstOrfail();

       if($user && !$user->confirmed) {
           $user->confirm();
           return redirect('/threads')->with('flash','You have successfully confirmed your account');
       }
       return redirect('/threads');
    }
}
