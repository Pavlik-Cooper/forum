<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Http\Requests\CreatePostRequest;
use App\Inspections\Spam;
use App\Reply;
use App\Rules\SpamFree;
use App\Thread;
use Illuminate\Http\Request;
use Gate;

class RepliesController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }
    public function index($channelID,Thread $thread){
        return $thread->replies()->paginate(2);
    }

    public function store($channelID,Thread $thread,CreatePostRequest $request){

            return $reply = $thread->addReply([
                'user_id'=> auth()->id(),
                'body'=> request('body')
            ])->load('owner');
    }

    public function update(Reply $reply){
        $this->authorize('update', $reply);

        try {
            $this->validate(request(),['body'=> ['required',new SpamFree]]);
            $reply->update(request(['body']));
        }catch (\Exception $e){
            return response('Sorry, your reply could not be saved',422);
        }

    }

    public function destroy(Reply $reply){
        $this->authorize('update', $reply);
        $reply->delete();
    }
//    public function validateReply(){
//        $this->validate(request(),['body'=> 'required']);
//        //or app()
//        resolve(Spam::class)->detect(request('body'));
//    }
}
