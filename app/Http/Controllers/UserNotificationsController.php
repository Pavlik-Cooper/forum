<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

class UserNotificationsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return auth()->user()->unreadNotifications;
    }
    public function destroy($user, $notification_id){
        auth()->user()->notifications()->findOrFail($notification_id)->markAsRead();
    }

}
