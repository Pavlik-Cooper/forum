<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Inspections\Spam;
use App\Rules\Recaptcha;
use App\Rules\SpamFree;
use App\Thread;
use App\Filters\ThreadFilters;
use App\Trending;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ThreadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    public function index(Channel $channel, ThreadFilters $filters, Trending $trending)
    {
        // TODO when a thread gets deleted, update $trending
        $threads = $this->getThreads($channel, $filters);
        $trending = $trending->get();
        return view('threads.index',compact('threads','trending'));
    }

    protected function getThreads(Channel $channel, ThreadFilters $filters)
    {
        $threads = Thread::latest()->filter($filters);
//        dd($threads->toSql());
        if ($channel->exists) {
            $threads->where('channel_id', $channel->id);
//           $threads = Thread::where('channel_id',$channel->id)->get();
        }
        return $threads->paginate(5);
    }

    public function create()
    {
        return view('threads.create');
    }

    public function store(Request $request, Spam $spam)
    {
        request()->validate([
           'title'=> ['required',new SpamFree],
           'body'=> ['required',new SpamFree],
            'channel_id'=> 'required|exists:channels,id',
            'g-recaptcha-response'=> [new Recaptcha()]
        ]);

        $thread = Thread::create([
            'title'=> $request->title,
            'body'=> request('body'),
            'user_id'=> auth()->id(),
            'channel_id'=> $request->channel_id
        ]);
        return redirect($thread->path())->with('flash','Yor thread has been published');
    }

    public function show($channelID,Thread $thread, Trending $trending)
    {
        if (auth()->check()) {
            auth()->user()->read($thread);
        }
        $trending->push($thread);

        $thread->visits()->record();

        return view('threads.show',[
            'thread' => $thread,
            'replies' => $thread->replies()->paginate(15)
        ]);

    }
    public function edit(Thread $thread)
    {
        //
    }

    public function update(Request $request, Thread $thread)
    {
        //
    }

    public function destroy($channel, Thread $thread)
    {
        //
        $this->authorize('update',$thread);

        $thread->delete();
        return redirect('/threads');
    }

}
