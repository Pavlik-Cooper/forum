<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar_path','confirmation_token'
    ];
    protected $casts = ['confirmed'=>'boolean'];

    public function getRouteKeyName()
    {
        return 'name';
    }

    protected $hidden = [
        'password', 'remember_token', 'email'
    ];
    public function threads(){
        return $this->hasMany(Thread::class)->orderBy('created_at','desc');
    }
    public function activities(){
        return $this->hasMany(Activity::class);
    }
    public function read($thread){
                                              // key     =>    value
        cache()->forever($this->visitedThreadCacheKey($thread), Carbon::now());
    }

    public function visitedThreadCacheKey($thread){
        return sprintf("users.%s.visits.%s", $this->id, $thread->id);
    }

    public function lastReply(){
        // the one most recent
        return $this->hasOne(Reply::class)->latest();
    }

    public function getAvatarPathAttribute($avatar)
    {
        return asset($avatar ? 'storage/' . $avatar : 'images/avatars/default-avatar.jpg');
    }
    public function confirm(){
        $this->confirmed = true;
        $this->confirmation_token = null;
        $this->save();
    }
    // don't do this
    public function isAdmin(){
        return in_array($this->name,['paul','Jhon Doe']);
    }
}
