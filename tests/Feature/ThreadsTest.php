<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ThreadsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function a_user_can_browse_tests()
    {
        $thread = factory('App\Thread')->create();
        $response = $this->get('/public/threads');

        $response->assertStatus(200);
        $response->assertSee($thread->title);
    }
}
