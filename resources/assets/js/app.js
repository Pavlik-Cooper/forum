require('./bootstrap');
import InstantSearch from 'vue-instantsearch';
window.Vue = require('vue');

let authorizations = require('./authorizations');

Vue.prototype.authorize = function (...params) {
    if (!window.App.signedIn) return false;

    if (typeof params[0] === 'string') {
        return authorizations[params[0]](params[1])
    }
    // return handler(user => user.id === whatever.user_id) (callback());
  return params[0](window.App.user);

};
Vue.prototype.signedIn = window.App.signedIn;

Vue.component('flash', require('./components/flash.vue'));
Vue.component('paginator', require('./components/paginator.vue'));
Vue.component('notification', require('./components/notification.vue'));
Vue.component('avatar-form', require('./components/avatar-form.vue'));

Vue.component('thread-view', require('./pages/thread.vue'));
Vue.component('wysiwyg', require('./components/wysiwyg.vue'));
Vue.use(InstantSearch);

const app = new Vue({
    el: '#app'
});
