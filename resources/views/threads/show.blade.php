@extends('layouts.app')
@section('header')
    <link rel="stylesheet" href="{{ asset('css/vendor/jquery.atwho.css') }}">
@endsection
@section('content')
    <thread-view :initial_rep_count="{{ $thread->replies_count }}" inline-template>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="card" style="margin-bottom: 20px;">
                        <div class="card-header">
                            <a href="{{ route('profile',$thread->creator->name) }}">{{ $thread->creator->name }}</a> posted:
                            <a href="{{ $thread->path() }}">{{$thread->title}}</a>
                            @can('update',$thread)
                                <form class="pull-right" action="{{ $thread->path() }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}
                                    <button class="btn btn-link">Delete thread</button>
                                </form>
                            @endcan
                        </div>
                        <div class="card-body">
                            {!! $thread->body !!}
                        </div>
                    </div>
                    <replies  @removed="RepliesCount--"
                              @added="RepliesCount++"
                    >
                    </replies>

                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <p>
                                This thread was published {{ $thread->created_at->diffForHumans() }} by
                                <a href="#">{{ $thread->creator->name }}</a>, and currently
                                has <span v-text="RepliesCount"></span> {{ str_plural('comment', $thread->replies_count) }}
                            </p>
                            <p>
                                <subscribe-button :active="{{ json_encode($thread->IsSubscribed)}}"></subscribe-button>
                                <button class="btn btn-outline-primary" @click="locked = true">Lock</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </thread-view>
@endsection
