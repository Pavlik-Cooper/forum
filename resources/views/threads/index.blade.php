@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
             @include('threads._list')
                <div class="row justify-content-center">
                        {{ $threads->render() }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header">
                        Trending threads
                    </div>
                    <div class="card-body">
                        <ul class="list-group">
                            @foreach($trending as $trend)
                                <a href="{{ url($trend->path) }}">{{ $trend->title }}</a>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
